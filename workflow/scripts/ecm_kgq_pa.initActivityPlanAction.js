function initActivityPlanAction(posicao){

    log.info("============================ initActivityPlanAction() ============================");
    var PROCESSO_PA = "ecm_kgq_apa";
    var userList = new java.util.ArrayList();
    var card = new java.util.HashMap();
    var matriculaUsu = getValue('WKUser'); // matrícula do usuário que irá abrir o subprocesso


    log.info("============================ PROCESSO A SER INICIADO: " + PROCESSO_PA + " ============================");
 
    var nomeResponsavelPA = hAPI.getCardValue("nmResponsavelPA");
    var nomeResponsavel = hAPI.getCardValue("nmResponsavelAtiv___" + posicao);
    var matriculaResponsavel  = hAPI.getCardValue("hiddenIndexMat___" + posicao);
    var oQueComo = hAPI.getCardValue("nmOqueComo___" + posicao);
    //var nmPrazoConclusao1 = hAPI.getCardValue("nmPrazoConclusao1___" + posicao);
    var prazoConclusao = hAPI.getCardValue("nmDtFinalPrev___" + posicao);
    var processo= getValue("WKNumProces").toString();
    var matResponsavelPA = hAPI.getCardValue("hiddenMatResponsavel");
    var nmPauta = hAPI.getCardValue("nmPauta");
    var nmStatus = hAPI.getCardValue("nmStatus___" + posicao);

    userList.add(matriculaResponsavel);

    log.info("USUARIO RESPONSAVEL PELA ATIVIDADE: " + nomeResponsavel);
    log.info("MATRICULA RESPONSAVEL PELA ATIVIDADE: " + matriculaResponsavel);
    log.info("O QUE COMO: " + oQueComo);
    log.info("PRAZO DE CONCLUSAO: " + prazoConclusao);

    var filtro = DatasetFactory.createConstraint("colleagueName", nomeResponsavel, nomeResponsavel, ConstraintType.MUST);
    var filtrando = new Array(filtro);
    var colaborador = DatasetFactory.getDataset("colleague", null, filtrando, null);

    card.put('hiddenMatResponsavel',colaborador.getValue(0,"colleaguePK.colleagueId"));
    card.put("cdPlanoAcao", processo);
    card.put("nmResponsavelPA", nomeResponsavelPA);
    card.put("nmOquCom", oQueComo);
    card.put("nmResponsavel", nomeResponsavel);
    //card.put('nmPrazoDeConclusao', nmPrazoConclusao1);
    card.put("nmPrazoDeConclusao", prazoConclusao);
    card.put("matResponsavelPA", matResponsavelPA);
    card.put("pauta", nmPauta);
    card.put("nmStatus", nmStatus);


    log.info("============================ INICIANDO PROCESSO DE ATIVIDADE ============================");
    hAPI.startProcess(PROCESSO_PA, 2, userList , "Iniciado automaticamente", true, card, true);
}



