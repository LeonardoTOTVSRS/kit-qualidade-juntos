function afterProcessCreate(processId){
    //coloca o valor do status como Ativo
    hAPI.setCardValue("status", "Ativo");
    //Teste de log do FLUIG
    log.info(">> afterProcessCreate");
    //coloca o valor do cdPlanoAcao e coloca o processId
    hAPI.setCardValue("cdPlanoAcao",processId);
    
    
  
    //--------------------------------------------------------------------------------//
	var processo     = String(getValue("WKNumProces")); // pegar o número do processo
	var nomeSubProc  = 'ecm_kgq_apa'; // colocar o nome do seu Subprocesso
	var matriculaUsu = 'teste'; // matrícula do usuário que irá abrir o subprocesso

    //Retorna um Mapa com todos os campos e valores do formulário da solicitação
    var campos   = hAPI.getCardData(processId);
	var contador = campos.keySet().iterator();

	var userList = new java.util.ArrayList();
	userList.add(matriculaUsu);
	var card     = new java.util.HashMap();

	var campos   = hAPI.getCardData(processId);
	var contador = campos.keySet().iterator();

    //LOOP pra pegar os campos do pai X filho
	while (contador.hasNext()) {
	    var id = contador.next();

	    var count = 1;
	    if (id.match(/nmOqueComo___/)) { // aqui você coloca o name de qualquer campo do seu Pai x Filho concatenado com "___", para poder percorrer ele
	        var campo = campos.get(id);
			//index
			var seq   = id.split("___")[1];

	        var a = card.put("nmOqueComo___" + count, campo.get("nmOqueComo___" + seq)); // aqui você defini o valor do Pai x Filho do processo principal para o Pai x Filho do Subprocesso
	       		
	        count++;
		}
		if (id.match(/nmStatus___/)) { // aqui você coloca o name de qualquer campo do seu Pai x Filho concatenado com "___", para poder percorrer ele
	        var campo = campos.get(id);
	        var seq   = id.split("___")[1];

	        var b = card.put("nmStatus___" + count, 'andamento');  // aqui você defini o valor do Pai x Filho do processo principal para o Pai x Filho do Subprocesso
	       	
	       			
	        count++;
		}
		
		
	}


	card.put('nmPrazoDeConclusao',hAPI.getCardValue(''));



	hAPI.startProcess("ecm_kgq_apa", 1, userList , "Solicitação inicializada pela função hAPI", true, card, true);
	
}