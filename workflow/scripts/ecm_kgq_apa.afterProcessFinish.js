function afterProcessFinish(processId){
	hAPI.setCardValue("status", "FINALIZADO");
	log.info("+++++++Finalizando processo de Atividades do Plano de Ação+++++++");
	
	var fullDate = new Date();
	var hours = fullDate.getHours();
	var minutes = fullDate.getMinutes();
	if (minutes <= 9){ minutes = "0" + minutes; }
	var timeValue = hours + ":" + minutes;
	var date = fullDate.getDate().toString();
	if(date.length == 1){ date = 0+date; }
	var mes = (fullDate.getMonth()+1).toString();	
	if(mes.length == 1){ mes = 0+mes; }
	var data = date+"/"+mes+"/"+fullDate.getFullYear();
	
	hAPI.setCardValue("dtConclusaoReal",data);
	
	
	
	
}