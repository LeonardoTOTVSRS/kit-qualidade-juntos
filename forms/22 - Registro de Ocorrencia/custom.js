//Start Jquery
$(function() {
	// numero de ocorrencia reincidente começa invisisvel
	var ocoRein = $("#ocorrencia_reinc").css("display","none");
	// div data efi comeca invisivel
	var divEfi = $("#divDtEficacia").hide();
	

	buildTab();
	//Verifica se o valor do checkbox ta marcado ou não
	$('input#fgContencao').change(function (e) { 
		e.preventDefault();
		if ($('input#fgContencao').is(':checked')) {
			$('input#fgContencao').val('checked')
		} else {
			$('input#fgContencao').val('')
		}
	});
	
//o que foi selecionado no zoom	
function setSelectedZoomItem(selectedItem) {
	var zoomSelecionado = selectedItem.inputName;
	var area = $("#nmArea").val();
	var origem = $("#nmOrigem").val();
	var ocoRein = $("#cdOcorrenciaReincidente").val();

		if(zoomSelecionado == "nmArea") {
			area = selectedItem['nmArea'];
		} else if(zoomSelecionado == "nmOrigem") {
			 origem = selectedItem['nmOrigem']
		} else if (zoomSelecionado == "addSolicReinc"){
			ocoRein = selectedItem['cdOcorrenciaReincidente']
		}
		  

}
//funcao auxiliar
function apagaCampo(campo){
	$(campo).val='';
}
//monta as tabs
function buildTab() {
	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).show(); //Fade in the active ID content
		return false;
	});
};

});
//Função paixfilho
function addFilhoTabelha() {
	var add = wdkAddChild('acoesTable');
	//Toda vez que criar um filho, ele coloca o calendário no campo
	$("#acoesTable").css('display','block');
	//corrigindo bug do pai x filho
	setInterval(function(){ 
		if($("[detailname='acoesTable']").length <= 1){
			$("#rowTabela").hide();
		}
	}, 0);
	$("#rowTabela").show();	
}
//deletar paixfilho
function fnCustomDelete(oElement){	
		var sub = fnWdkRemoveChild(oElement);
		FLUIGC.toast({ title:'Fluig' , message:'Ação Excluida !' , type:'danger'});
 }

 