function displayFields(form,customHTML){
	//Desabilita os campos do form
	form.setShowDisabledFields(true);
	//Fz com que o botao da impressora desapareça
	form.setHidePrintLink(true);
	///Pega o Status da Atividade
	var numActivity = getValue("WKNumState");
	///Pega o Número do Processo
	var numprocesso = getValue("WKNumProces");


	//Setando número da atividade
	var INICIO = 2;
	var NOTIFICA = 4;
	
	//Desabilita alguns campos do form
	form.setEnabled("cdPlanoAcao",false);
	form.setEnabled("nmResponsavelPA",false);
	form.setEnabled("nmOquCom",false);
	form.setEnabled("nmResponsavel",false);
	form.setEnabled("nmPrazoDeConclusao",false);
	
	
	//Se for atividade 2
	if(numActivity == INICIO){
		
		//Preenche o campo cdAtividadePA com o número do processo
		form.setValue("cdAtividadePA", numprocesso);
		
		customHTML.append('$(function(){');
		customHTML.append('$("#dsAtividade").removeAttr("readonly");');
		customHTML.append('});');
		form.setEnabled("dsAtividade",true);
		
	}

	//Atividade de número 0 ou 1	
	if(numActivity == 0 || numActivity == 1 ){
		var linkPA = form.getValue("cdPlanoAcao");
		form.setValue("cdAtividadePA", numprocesso);
		

		//Testes no log do FLUIG
		log.info(linkPA);
		log.info("verificando se está nulo" + linkPA==null);
		log.info("verificando se está vazio" + linkPA=='');
		
		//Rever este teste aqui
		if(linkPA == '' || linkPA == null){
			customHTML.append('<script>');
			customHTML.append('$(function(){');
			customHTML.append('$("#goToOccurrence").remove();');
			customHTML.append('});');
			customHTML.append('</script>');
		}
	}

	

	if(numActivity == INICIO){
		//form.setValue("status", 'Andamento');
		form.setValue("cdAtividadePA", numprocesso);
		
	}
	if(numActivity == NOTIFICA){
		form.setValue("cdAtividadePA", numprocesso);
		//form.setValue("status", 'Validação');
		

	}

};