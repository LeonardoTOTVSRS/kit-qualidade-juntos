function validateForm(form){

    log.info("======================validateForm======================" );

    var nrAtividade = getValue("WKNumState");
    
    var msg = '';
    var nmAssunto = form.getValue('nmAssunto')
    var nmParticipantes = form.getValue('nmParticipantes')
    var nmPauta = form.getValue('nmPauta')
    var nmDefinicoes = form.getValue('nmDefinicoes')
    var nmDtInicial = form.getValue('nmDtInicial')
    var nmPrazoConclusao1 = form.getValue('nmPrazoConclusao1')
    var nmDtFinalReal = form.getValue('nmDtFinalReal')
    var nmOqueComo = form.getValue('nmOqueComo')

    //Se a atividade for Início
    if (nrAtividade == 0 ) {             
        
        //Validação no Pai x Filho
        var indexes = form.getChildrenIndexes("ativPlano");
        if (indexes.length > 0) {
            for (var i = 0; i < indexes.length; i++) { // percorre os campos Pai x Filho
                if(form.getValue('nmOqueComo___' + indexes[i]) == null || form.getValue('nmOqueComo___' + indexes[i]) == '') {
                    throw "O campo 'O que ? Como ?'  é de preenchimento obrigatório\n";
                }
                if(form.getValue('nmResponsavelAtiv___' + indexes[i]) == null || form.getValue('nmResponsavelAtiv___' + indexes[i]) == '') {
                    throw "Informe o valor do campo de Responsável!";
                }
                var index = i;
                var indexValue =i.value;

                for(var x = 0; x < indexes.length; x++){
               

                    if(form.getValue('nmResponsavelAtiv___' + indexes[x]) == indexValue && x != index)
                    {
                        throw "Nome do campo não pode ser igual !";
                    }
                }
            }
        }


        if (nmAssunto == null || nmAssunto == undefined) {
			msg+= "O campo Assunto é de preenchimento obrigatório\n";
        }
        
        // Validação dos Participantes
        if (nmParticipantes == null || nmParticipantes == undefined) {
			msg+= "O campo Participantes é de preenchimento obrigatório\n";
        }

        if (nmPauta == null || nmPauta == undefined || nmPauta == '') {
			msg+= "O campo Pauta é de preenchimento obrigatório\n";
        }
        if (nmDefinicoes == null || nmDefinicoes == undefined || nmDefinicoes == '') {
			msg+= "O campo Definições é de preenchimento obrigatório\n";
        }
        
        // Validação das Datas
        // if (nmDtInicial == null || nmDtInicial == undefined || nmDtInicial == '') {
		// 	msg+= "O campo Data inicial é de preenchimento obrigatório\n";
        // }
        // if (nmPrazoConclusao1 == null || nmPrazoConclusao1 == undefined || nmPrazoConclusao1 == '') {
		// 	msg+= "O campo Data Final Prev é de preenchimento obrigatório\n";
        // }
        // if (nmDtFinalReal == null || nmDtFinalReal == undefined || nmDtFinalReal == '') {
		// 	msg+= "O campo Data Final Real é de preenchimento obrigatório\n";
        //}
       
    } 
    
    if(msg != ""){
        throw msg;
        
    }
}