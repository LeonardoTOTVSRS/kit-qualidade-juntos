function displayFields(form, customHTML){
	
log.info("======================displayFields======================" );

//mostra os campos desabilitados
form.setShowDisabledFields(true);
//faz o botao de imprimir desaparecer
form.setHidePrintLink(true);

//Número da Atividade
var numActivity = getValue("WKNumState"); //3
//Número de Processo
var numSolic = getValue("WKNumProces"); //1175

//Teste de Log Fluig
log.info("Numero da Atividade do Plano de Acao: " + numActivity + ":" + numSolic);

//Array Vazio
var ArrayStatus = [];
//Variavel Vazia
var count = 0;

if (numActivity == 0 || numActivity == 1) {

//Traz nome do usuário pela matícula
var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
var constraints = [c1];
var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
//prernche o campo com os dados puxados do dataset
form.setValue('nmResponsavelPA', colaborador.getValue(0, "colleagueName"));
form.setValue('hiddenMatResponsavel', getValue("WKUser"));
//teste no log do fluig
log.info("Responsavel: " + getValue("WKUser") + " " + colaborador.getValue(0, "colleagueName"));
form.setValue('status', 'Inicio');

	}

//Verificação em caso da atividade ser 0
if (numActivity == 0){
	
form.setValue('envio', 'manual');
	
	
}
//Verificação em caso da atividade ser 3

if(numActivity == 3){
form.setValue('status', 'Gerencia');

}
//Verificação em caso da atividade ser 4

if(numActivity == 4){
form.setValue('status', 'Eficacia');

}
//Verificação em caso da atividade ser 5
if(numActivity == 5){
form.setValue('status', 'Finalizado');

}


//Verificação em caso da atividade ser 3,4,5

if (numActivity == 3 || numActivity == 4 || numActivity == 5 ) {
	//Abre o Pai e Filho
	customHTML.append("<script>$('#ativPlano').css('display','block');</script>");
	customHTML.append("<script>$('#btIncluir').css('display','none');</script>");
	
	//Desabilita os campos do pai e filho nessa atividade
	(function desabilitaPaiFilho(){
		
		var x = 1;
		var rows = parseInt(form.getValue("hiddenLinhasGrid"));
		
		while (x <= rows) {
			var field = form.getValue("nmOqueComo___" + x);
			if (field == null) { 
				x = x + 1;
				rows = rows + 1;
			} else {
				form.setEnabled('nmOqueComo___' + x, false);	
				form.setEnabled('nmResponsavelAtiv___' + x, false);
				form.setEnabled('nmPrazoConclusao___' + x, false);
				form.setEnabled('nmDtInicial___' + x, false);
				form.setEnabled('nmPrazoConclusao1___' + x, false);
				form.setEnabled('nmDtFinalReal___' + x, false);
				form.setEnabled('nmStatus___' + x, false);
				form.setEnabled('nmDtFinalPrev___' + x, false);
		

				x = x + 1;
			}
		}


	})();

	desabilitarCampos(["nmPauta","nmDefinicoes","nmAssunto", "nmParticipantes, nmDtFinalPrev"])
	
	//Traz status das atividades
	var constraint1 = DatasetFactory.createConstraint("cdPlanoAcao", numSolic, numSolic, ConstraintType.MUST);
	var constraint2 = DatasetFactory.createConstraint("metadata#active", "true", "true",ConstraintType.MUST);
		
	log.info("Pesquisa");	
	var constraints = new Array(constraint1, constraint2);
	log.info("Constraints");	
	var sortingFields = new Array("cdAtividadePA");
	var atividades = DatasetFactory.getDataset("ecm_kgq_atividadepa", null, constraints, sortingFields);	
	log.info("Numero de atividades do plano de acao: " + atividades["values"].length); //1
	

	// Posiciona os status das atividades nas linhas corretas da respectiva atividade
	var z = 1;

	//1
	for(var i=0; i < atividades.rowsCount; i++) {
		var state = atividades.getValue(i, "status"); 
		
		var dtConclusaoReal = "";

		 if (state !== null || state !== undefined) {
			dtConclusaoReal = atividades.getValue(i, "dtConclusaoReal");				
		}
		
		
		log.info("Numero Atividade PA smug: " + atividades.getValue(i, "cdAtividadePA")); //ta dando pal null here
		
		
		while (z) {
			var campoCont = form.getValue("nmStatus___" + z);
			if (campoCont == null) {
				z = z + 1;
			} else {
				form.setValue("nmStatus___" + z, state);
				form.setValue("nmDtFinalReal___" + z, dtConclusaoReal);
				log.info("Estado da AtividadePA sadiadomal: " + atividades.getValue(i, "status"));
				
				if (state == "") {
					ArrayStatus[count, count] = ['nmStatus___' + z, state];
					//log.info("valor do ARRAY " + ArrayStatus[count, count]);
				} else {
					ArrayStatus[count, count] = ['nmStatus___' + z, "Ativo"];  
					//log.info("valor do ARRAY " + ArrayStatus[count, count]);
				}

				z = z + 1;
				count = count + 1;
				
				break;
			}
		}

	}
}



	//Retorna n?mero da atividade dentro do HTML
	customHTML.append("<script>function getWKNumState(){ return " + getValue("WKNumState") + "; }</script>");

	
	//funcao para desabilitar campos
	function desabilitarCampos(campos) {
		campos.forEach(function(campo) {
			form.setEnabled(campo, false);
		});
	}
}
