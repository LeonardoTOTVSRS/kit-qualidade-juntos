$(document).ready(function(){
	//inicializa o campo de data
	(function inicializaData() {
		var data = new Date();
		var dia = data.getDate();
		if (dia < 10) {
			dia = "0" + dia;
		}
		var mes = data.getMonth() + 1;
		if (mes < 10) {
			mes = "0" + mes;
		}
		var ano = data.getFullYear();
		var dataFormatada = dia + "/" + mes + "/" + ano;

	$("#nmDataPA").val(dataFormatada);
	})();
});
// $(document).on("click", "#goToOccurrence", function(e) {
// 	e.preventDefault();
// 	openOccurrence();
// });

//Função inicializar
function init() {
	var INICIO_SOLICITACAO = "0",
		ATIVIDADE_INICIO = "1";
		var activity = getWKNumState();


	// showRedirectButton();
	if (activity != ATIVIDADE_INICIO && activity != INICIO_SOLICITACAO) {
		enableButtons('false', document.getElementById("formPA"));
		if (activity != null) {
			var tableGrid = document.getElementById("ativPlano");
			trashDisappearsGrid(tableGrid);
		}
	} else if (activity == ATIVIDADE_INICIO) {
		disableTrashOldActivities();
	};
}

//Adiciona o pai x filho com calendar dentro
function addFilhoTabelha() {
	var add = wdkAddChild('ativPlano');
	//Toda vez que criar um filho, ele coloca o calendário no campo
	FLUIGC.calendar("#nmDtInicial___" + add);
	FLUIGC.calendar("#nmDtFinalPrev___" + add);
	FLUIGC.calendar("#nmDtFinalReal___" + add);	
	$("#ativPlano").css('display','block');
	console.log(add);
	setInterval(function(){ 
		if($("[detailname='ativPlano']").length <= 1){
			$("#rowTabela").hide();
		}
	}, 0);
	$("#rowTabela").show();	
}
 
function disableCheckbox(checkboxName) {
	var INICIO_SOLICITACAO = "0";
	var ATIVIDADE_INICIO = "1";
	var activity = getWKNumState();

	if (activity != INICIO_SOLICITACAO && activity != ATIVIDADE_INICIO) {
		var estado = eval("document.form." + checkboxName + ".checked");
		if (estado == true) {
			eval("document.form." + checkboxName + ".checked = false");
		} else {
			eval("document.form." + checkboxName + ".checked = true");
		}
	}
}
//pega o id e a matricula do usuario selecionado no input Responsavel
function setSelectedZoomItem(selectedItem) {
	console.log(selectedItem);
	if(selectedItem.inputName.startsWith('nmResponsavelAtiv___')){
		var index1 = selectedItem.inputId;
		var index = index1.split("___");
		var a = $("#idResponsavelAtiv___"+index[1]).val(selectedItem.colleagueId);
		var b = $("#nmResponsavelAtiv___"+index[1]).val(selectedItem.colleagueName);	
	}
	if (selectedItem == 'nmParticipantes'){
		document.getElementById("nmParticipantes").value = selectedItem['colleagueName'] + ';'; 
	}
}



//Função auxiliar
function disableTrashOldActivities() {
	var qtRows = document.getElementById("ativPlano").rows.length - 2;
	var x = 1;
	var indexExists = 1;
	while (indexExists <= qtRows) {
		var campo = document.getElementById("nmOqueComo___" + indexExists);
		if (campo == null) {
			indexExists = indexExists + 1;
			qtRows = qtRows + 1;
		} else {
			document.images[x].style.visibility = "hidden";
			x = x + 1;
			indexExists = indexExists + 1;
		}
	}
}

//Função para abrir a busca de ocorrência
// (function openOccurrence() {
// 	//número da ocorrência e joga numa variavel
// 	var numRO = document.getElementById("cdOcorrencia").value;
// 	//número da empresa que ta logado
// 	var numEmpresa = window.parent.WCMAPI.getTenantId();
// 	//nome do usuario logado
// 	var usuarioLogado = window.parent.WCMAPI.getUserCode();

// 	//Variaveis vazias
// 	var NrDocto = "";
// 	var versaoDoc = "";

// 	var arr = new Array(DatasetFactory.createConstraint("processAttachmentPK.processInstanceId", numRO, numRO, ConstraintType.MUST));
// 	//Pega o dataset
// 	var ProcessRO = DatasetFactory.getDataset("processAttachment", null, arr, null);
// 	//Se tiver resultados ele pega o número do documento
// 	if (ProcessRO.values.length > 0) {
// 		NrDocto = ProcessRO.values[0]["documentId"];
// 	}
// 	//Consulta o dataset document passando os filtros
// 	var arr2 = new Array(DatasetFactory.createConstraint("documentPK.documentId", NrDocto, NrDocto, ConstraintType.MUST));
// 	var ArrayDoc = DatasetFactory.getDataset("document", null, arr2, null);

// 	//Se tiver resultados ele pega o documentPK.version
// 	if (ArrayDoc.values.length > 0) {
// 		versaoDoc = ArrayDoc.values[0]["documentPK.version"];
// 	}

// 	//Testa o console o número da ocorrencia, nome da empresa e numero do documento
// 	console.log(numRO, numEmpresa, NrDocto);

// 	//Função maluca pra montar o url e abrir uma tela com outras informações
// 	var url =
// 		window.location.origin +
// 		'/portal/p/' +
// 		numEmpresa +
// 		'/pageworkflowview?app_ecm_workflowview_processInstanceId=' + numRO +
// 		'&app_ecm_workflowview_currentMovto=2' +
// 		'&app_ecm_workflowview_taskUserId=' + usuarioLogado +
// 		'&app_ecm_workflowview_managerMode=false' +
// 		'&app_ecm_workflowview_nrDoc=' + NrDocto +
// 		'&app_ecm_workflowview_versionDoc=' + versaoDoc;

// 	/*	  portal/p/1/pageworkflowview?app_ecm_workflowview_processInstanceId=53&app_ecm_workflowview_currentMovto=2&app_ecm_workflowview_taskUserId=admin.kgq&app_ecm_workflowview_managerMode=false&app_ecm_workflowview_nrDoc=114&app_ecm_workflowview_versionDoc=1000	  
		  
// 		  portal/p/1/pageworkflowview?app_ecm_workflowview_detailsProcessInstanceID=53&app_ecm_workflowview_taskUserId=admin.kgq	  */
	
// 	 //Função maluca pra montar o url e abrir uma tela com outras informações
// 	var url2 =
// 		window.location.origin +
// 		'/portal/p/' +
// 		numEmpresa +
// 		'/pageworkflowview?app_ecm_workflowview_detailsProcessInstanceID=' + numRO +
// 		'&app_ecm_workflowview_taskUserId=' + usuarioLogado;

// 	//Função maluca pra montar o url e abrir uma tela com outras informações
// 	window.open(url2, "Registro de Ocorrência", "toolbar=no,scrollbars=yes,resizable=yes,width=1024,height=768");
// })();

//Função que não faz sentido
function getIndexGrid(buttonSelected) {
	var indexInit = buttonSelected.id.indexOf('__') + 3;
	var size = buttonSelected.id.length;
	var number = buttonSelected.id.substring(indexInit, size);
	console.log(number);
	return number;
}

//Função que não faz sentido
function trashDisappearsGrid(tableGrid) {
	var tBody = tableGrid.getElementsByTagName("tbody")[0];
	var tds = tBody.getElementsByTagName("td");
	var trs = tBody.getElementsByTagName("tr");
	var rowsTBody = tBody.rows.length;
	var tdsFirstRow = tds.length / rowsTBody;
	var indexTD = 0;

	for (var i = 1; i < rowsTBody; i++) {
		indexTD += tdsFirstRow;
		tds[indexTD].style.visibility = 'hidden';
	}
}

//Função que não faz sentido
function clearInputFromTable(object, fieldId) {
	var index = getIndexGrid(object);
	var objectDate = document.getElementById(fieldId + index);
	objectDate.value = "";
}


// function showRedirectButton() {
// 	var INICIO_SOLICITACAO = "0";
// 	var activity = getWKNumState();
// 	var numOcorrencia = document.getElementById("cdOcorrencia").value;
// 	if (activity == INICIO_SOLICITACAO || numOcorrencia == "" || numOcorrencia == "0") {
// 		document.getElementById("goToOccurrence").style.visibility = "hidden";
// 	} else {
// 		document.getElementById("goToOccurrence").style.visibility = "visible";
// 	}
// }


//Função que apaga campo
function clearField(field) {
	document.getElementById(field).value = '';
}

