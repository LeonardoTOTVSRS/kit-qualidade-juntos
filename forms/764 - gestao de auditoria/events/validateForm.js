function validateForm(form) {
	
	var nrAtividade = getValue("WKNumState");

	if (nrAtividade !== null || nrAtividade !== undefined || nrAtividade !== '') {	
		//Variaveis
			var solicitante = form.getValue('nmSolicitante')
			var comunicado =  form.getValue('dsComunicado')
			var auditoria = form.getValue('fgTipoAuditoria')
			var objAuditoria = form.getValue('dsObjetivoAuditoria')
			var data = form.getValue('dtDataInicioAudit')
			var dataFimm = form.getValue('dtDataFimAudit')
			var area = form.getValue('nmArea')
			var nmAuditor = form.getValue('nmAuditor')
			var obsPla = form.getValue('nmObservacoesPlanejamento')
			var msg = '';
			
		//Validações
		if (solicitante == null || solicitante == undefined) {
			msg+= "O campo Solicitante é de preenchimento obrigatório\n";
		}
		
		if (comunicado == "" ){
			msg+= "O campo Comunicado é de preenchimento obrigatório\n";
		}
		if (auditoria == ""){
			msg+= "O campo Tipo de Auditoria é de preenchimento obrigatório\n";
		}
		if (objAuditoria == ""){
			msg+= "O campo Objetivo da Auditoria é de preenchimento obrigatório\n";
		}
		if (data == "" || dataFimm == "") {
			msg+= "O campo Período de Auditoria é de preenchimento obrigatório\n";
		}
		
		
		if (area == '') {
			log.info("======================ErroArea======================")
			msg+= "O campo Area é de preenchimento obrigatório \n" + '' + area;
		}

		// if (nmAuditor == '') {
		// 	log.info("======================ErroAuditor======================")
		// 	msg+= "O campo Auditor é de preenchimento obrigatório\n";
		// }

		// if (obsPla == '') {
		// 	log.info("======================ErroObs======================")
		// 	msg+= "O campo Observações é de preenchimento obrigatório\n";
		// }
		
		
		//Validação da data 
		var dataInicio = form.getValue("dtDataInicioAudit").split('/');
		var dataFim = form.getValue("dtDataInvertFimAudit").split('/');
		
		if ( dataInicio[2] > dataFim[2] )
			msg+= "A data fim não pode ser menor que a data de início.\n";
		
		else if ( dataInicio[1] > dataFim[1] )
			msg+= "A data fim não pode ser menor que a data de início.\n";
			
		else if ( dataInicio[0] > dataFim[0] )
			msg+= "A data fim não pode ser menor que a data de início.\n";
				
	}
	//IMPRIME MENSAGEM DE ERRO			
	if(msg != ""){
		throw msg;
	}
}