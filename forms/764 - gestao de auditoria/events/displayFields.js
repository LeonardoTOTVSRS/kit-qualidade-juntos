function displayFields(form, customHTML) {
	log.info("====================== DISPLAY FIELDS ======================");
	form.setShowDisabledFields(true);
	form.setHidePrintLink(true);
	
s
	if (form.getValue("fgTipoAuditoria") == "") {
		form.setValue('fgTipoAuditoria', 'fgTipoAuditInt');
	}

	var numActivity = getValue("WKNumState");
	//Constantes das Atividades
	var INICIO_SOLICITACAO = 0;
	var ATIVIDADE_INICIAL = 1;
	var DETERMINAR_OBJETIVOS = 2;
	var AUTOMATICA = 3;
	var NOTIFICACAO = 4;
	var ANALISE_AUDITORIA = 5;
	var AVALIACAO_QUALIDADE = 6;
	var CONVOCACAO_REUNIAO = 7;
	var FIM = 8;

	//Listas de Campos
	var campos1 = ["dtData","dsAcoesExecutadas","dsObservacoesAvaliacao","nmRespQualidade","nmMatricRespQualidade","rdAuditRealizada","dtDataInicioReplanAudit","dtDataFimReplanAudit","dsObservacoesQualidade","nmRespAuditLider","nmMatricRespLider"];
	var campos2 = ["nmArea","dtData","dsComunicado","dtDataInicioAudit","dtDataFimAudit","dsObjetivoAuditoria","dsAcoesExecutadas","dsObservacoesAvaliacao","nmRespQualidade","rdAuditRealizada","dtDataInicioReplanAudit","dtDataFimReplanAudit","nmMatricRespQualidade","dsObservacoesQualidade","nmRespAuditLider","nmMatricRespLider","fgTipoAuditoria","nmSolicitante","dtDataInvertFimAudit","nmObservacoesPlanejamento","nmAuditor"];		
	var campos3 = ["nmArea","dtData","dsComunicado","dtDataInicioAudit","dtDataFimAudit","dsObjetivoAuditoria","dsAcoesExecutadas","dsObservacoesAvaliacao","nmRespQualidade","rdAuditRealizada","dtDataInicioReplanAudit","dtDataFimReplanAudit","nmMatricRespQualidade","dsObservacoesQualidade","nmRespAuditLider","nmMatricRespLider","fgTipoAuditoria"];		
	var campos4 = ["nmSolicitante","dtDataInvertFimAudit","nmAuditor","nmObservacoesPlanejamento"];
	var campos5 = ["nmSolicitante","dtDataInvertFimAudit","nmAuditor","nmObservacoesPlanejamento"];


	//Quando estiver na atividade de inicio
	if (numActivity == INICIO_SOLICITACAO || numActivity == ATIVIDADE_INICIAL) {
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
		var constraints = new Array(c1);
		var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
		var colaboradorLogado = getValue("WKUser");
		form.setValue('nmResponsavel', colaborador.getValue(0, "colleagueName"));
		form.setValue('nmMatricResponsavel', colaboradorLogado);

		
		(function fullDate() {
			var fullDate = new Date();
			var hours = fullDate.getHours();
			var minutes = fullDate.getMinutes();
			if (minutes <= 9) {
				minutes = "0" + minutes;
			}
			var timeValue = hours + ":" + minutes;
			var date = fullDate.getDate().toString();
			if (date.length == 1) {
				date = 0 + date;
			}
			var mes = (fullDate.getMonth() + 1).toString();
			if (mes.length == 1) {
				mes = 0 + mes;
			}
			var data = date + "/" + mes + "/" + fullDate.getFullYear();
	
			form.setValue('dtData', data);
			form.setValue('dtDataInicioAudit', data);
			form.setValue('dtDataFimAudit', data);
		})();
		
	//desabilita alguns campos e habilia outro
		desabilitarCampos(campos1);
		form.setEnabled("dsObjetivoAuditoria", true);
		
	}

	if (numActivity == DETERMINAR_OBJETIVOS) {
		desabilitarCampos(campos2);
		form.setEnabled("nmAuditor", false);
		form.setEnabled("nmObservacoesPlanejamento", false);
		
	} else if (numActivity == AUTOMATICA) {
		desabilitarCampos(campos2);
		form.setEnabled("nmAuditor", false);
		form.setEnabled("nmObservacoesPlanejamento", false);

	} else if (numActivity == NOTIFICACAO) {
		desabilitarCampos(campos3);
		
	} else if (numActivity == ANALISE_AUDITORIA) {
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
		var constraints = new Array(c1);
		var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
		var colaboradorLogado = getValue("WKUser");
		form.setValue('nmRespAuditLider', colaborador.getValue(0, "colleagueName"));
		form.setValue('nmMatricRespLider', colaboradorLogado);

		desabilitarCampos(campos3);
		desabilitarCampos(campos4);
	
	} else if (numActivity == AVALIACAO_QUALIDADE) {
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
		var constraints = new Array(c1);
		var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
		var colaboradorLogado = getValue("WKUser");
		form.setValue('nmRespQualidade', colaborador.getValue(0, "colleagueName"));
		form.setValue('nmMatricRespQualidade', colaboradorLogado);

		desabilitarCampos(campos3)
	} else if (numActivity == CONVOCACAO_REUNIAO) {
		desabilitarCampos(campos3);
		desabilitarCampos(campos5);

		
	} else if (numActivity == FIM) {
		desabilitarCampos(campos3);
		desabilitarCampos(campos5);

	} else if (numActivity == null) {
		(function iterarCard() {
			var habilitar = false; // Informe True para Habilitar ou False para Desabilitar os campos
				var mapaForm = new java.util.HashMap();
				mapaForm = form.getCardData();
				var it = mapaForm.keySet().iterator();
				
				while (it.hasNext()) { // Laço de repetição para habilitar/desabilitar os campos
					var key = it.next();
					form.setEnabled(key, habilitar);
				}
		})();
	}

	form.setEnabled("nrSolicitacao", false);
	form.setEnabled("nmResponsavel", false);
	form.setEnabled("nmMatricResponsavel", false);

	customHTML.append("<script>function getWKNumState(){ return " + getValue("WKNumState") + "; }</script>");

		///Funções Auxiliares
		function desabilitarCampos(campos) {
		campos.forEach(function(campo) {
			form.setEnabled(campo, false);
		});
		}
		function habilitarCampos(campos){
		campos.forEach(function(campo){ 
			form.setEnabled(campo, true); 
		});		
		}
		function esconderDIV(ocultadiv) {
		customHTML.append("$('" + ocultadiv + "').hide();");
		}
		function mostrarDIV(mostradiv) {
		customHTML.append("$('" + mostradiv + "').show();");
		}
		


}