function displayFields(form,customHTML){
	
	form.setShowDisabledFields(true); 
	form.setHidePrintLink(true);
	
	var numActivity = getValue("WKNumState");
	//Atividades
	var INICIO_SOLICITACAO = 0;
	var FAZER_TRIAGEM = 2;

	//Campos do Formulario em lista
	var campos1 = ["fgDistCopControl","fgNecessitaCapacit","nmVersao","nmDocumento","dsMotivo","fgTipoSolicitacao"];
	var campos2 = ["fgTipoSolicitacao","dsMotivo","nmDocumento","nmVersao","fgNecessitaCapacit","fgDistCopControl","dsComentTriagem"];

	
	//Atividade inicial
	if(numActivity == INICIO_SOLICITACAO){
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
		var constraints = new Array(c1);
		var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
		var colaboradorLogado = getValue("WKUser");
		form.setValue('nmSolicitante',colaborador.getValue(0,"colleagueName"));
		form.setValue('nmMatricSolicitante',colaboradorLogado);
		//funcao auxiliar para data
		(function inicializaData() {
			var date = new Date();
		var Dia = date.getDate();
		var month = date.getMonth()+ 1;
		var minutos = date.getMinutes();
        
        if (Dia.toString().length <= 1) {
        	Dia = '0' + Dia;
        }
        
        if (month.toString().length <= 1) {
        	month = '0' + month;
        }
        
        if (date.getMinutes().toString().length <= 1) {
        	minutos = '0' + minutos;
        }

        var dataAtual = Dia + "/" + month + "/" + date.getFullYear() + " " + date.getHours() + ":" + minutos;

		form.setValue('dtData',dataAtual);
		})();
		//habilita um campo do form
		form.setEnabled("dsComentTriagem",false);
	}
	//Atividade de Triagem
	if(numActivity == FAZER_TRIAGEM){
		
		var revisar = form.getValue("fgTipoSolicitacao");
		if(revisar == "fgTipoSolicRevis" ){
			customHTML.append("<script type='text/javascript'> $('#div_revisar').show();</script>"); 
			}
		else{
			}
		desabilitarCampos(campos1);
		form.setEnabled("dsComentTriagem", true);
	}
	
	if(numActivity != INICIO_SOLICITACAO && numActivity != FAZER_TRIAGEM && numActivity != null ){
		desabilitarCampos(campos2);
		
		var revisar = form.getValue("fgTipoSolicitacao");
		if(revisar == "fgTipoSolicRevis" ){
			customHTML.append("<script type='text/javascript'> $('#div_revisar').show();</script>"); 
			}
		else{
			}
		
	}

	form.setEnabled("nmSolicitante",false);	
	form.setEnabled("nmMatricSolicitante", false);
	form.setEnabled("dtData", false);
	
	customHTML.append("<script>function getWKNumState(){ return " + getValue("WKNumState") + "; }</script>");

	///Funções Auxiliares
	function desabilitarCampos(campos) {
		campos.forEach(function(campo) {
			form.setEnabled(campo, false);
		});
	}
	function habilitarCampos(campos){
		campos.forEach(function(campo){ 
			form.setEnabled(campo, true); 
		});		
	}
	function esconderDIV(ocultadiv) {
		customHTML.append("$('" + ocultadiv + "').hide();");
	}
	function mostrarDIV(mostradiv) {
		customHTML.append("$('" + mostradiv + "').show();");
	}

}